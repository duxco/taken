var auth = require("./auth"),
	bodyParser = require('body-parser'),
	dbcon = require("./dbcon/dbcon"),
	express = require("express"),
	fs = require("fs"),
	path = require("path");

var databases = dbcon(),
	dirName = "api",
	dirPath = path.join(__dirname, dirName),
	router = express.Router();

router.use(bodyParser.json());

fs.readdirSync(dirPath).forEach(function( file ){
	if( file.indexOf("_.") == -1 ){
		var api = require("./" + dirName + "/" +file)( router, databases, auth );
		router.use("/api", api);
	}
});

// Files managment
router.use("/uploads/public", express.static("../uploads/public"));
router.use(/^\/download\/uploads\/public\/.*/, function(req, res){
	var path = req.baseUrl.split("/");
	path.splice(0, 2);
	path = path.join("/");

	res.download("../" + path);
});

// 404 Not found handler
router.use("/api/*", function( req, res ){
	res.status(404).send("404 API not found");
});

module.exports = router;